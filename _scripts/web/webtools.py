import pandas as pd
import glob
import hvplot.pandas
import holoviews as hv
from bokeh.themes.theme import Theme
import os

class webtools:

    def theme_it():
        '''Define styling options directly to the Bokeh backend.'''
        theme = Theme(
            json={
                'attrs': {
                    'Figure': {
                        'outline_line_width': 1,
                        # 'border_fill_color': '#f8f8f8',
                        'outline_line_color': '#000000',
                        # 'background_fill_color': '#f8f8f8',
                    },
                    'Grid': {
                    },

                    'Axis': {
                    }
                }
            })

        hv.renderer('bokeh').theme = theme

    def create_Linac3_plots(directory):
        '''Looks for all XLS files in the different directories and produces the plots suggested by Giulia.'''
        files = sorted(glob.glob(f'{directory}*.XLS'))

        data = {}

        if len(files) > 0:
            for f in files:
                data[f[-7:-4]] = pd.read_csv(f, sep='\t')

            # common options for all plots
            options = hv.opts(height=300,
                              active_tools=['box_zoom'],
                              fontsize={
                              'title': 10,
                              'labels': 10,
                              'xticks': 10,
                              'yticks': 10})

            # transmission and energy plots
            trans = data['AVG'][['Length [m]', 'Alive [%]']].hvplot(kind='line', x='Length [m]', responsive=True).opts(options)
            nrg = data['AVG'][['Length [m]', 'Kinetic Energy Average [GeV]']].hvplot(kind='line', x='Length [m]', responsive=True).opts(
                options)
            elements = webtools.plot_lattice_elements(data['AVG'])

            plot = (elements + trans.relabel('Transmission') + nrg.relabel('Energy')).cols(1).opts(toolbar='right')
            html_file = f'{directory}transmission_energy.html'
            hv.save(plot, html_file, backend='bokeh', toolbar=True)
            print(f'Created file {html_file}.')

            # plotting RMS quantities

            dataset = data['RMS'][["Length [m]", "(X,BGX') RMS-Emittance [m.rad]", "(Y,BGY') RMS-Emittance [m.rad]"]]
            temit = dataset.hvplot(kind='line', x='Length [m]', responsive=True).opts(options).opts(
                ylabel='Transverse RMS emittance [m.rad]',legend_position='top_left')

            dataset = data['RMS'][['Length [m]', "(PHI,dP) RMS-Emittance [deg.MeV/c]"]]
            lemit = dataset.hvplot(kind='line', x='Length [m]', responsive=True).opts(options)

            plot = (temit.relabel('Transverse RMS emittance')
                    + lemit.relabel('Longitudinal RMS emittance')).cols(1).opts(toolbar='right')

            html_file = f'{directory}RMS_emittance.html'
            hv.save(plot, html_file, backend='bokeh', toolbar=True)
            print(f'Created file {html_file}.')

            dataset = data['RMS'][['Length [m]', 'x RMS [m]', 'y RMS [m]']]
            envel = dataset.hvplot(kind='line', x='Length [m]', responsive=True).opts(options).opts(
                ylabel='RMS beam size [m]')

            plot = envel.relabel('RMS beam size').opts(legend_position='top_left').opts(toolbar='right')

            html_file = f'{directory}RMS_beam_size.html'
            hv.save(plot, html_file, backend='bokeh', toolbar=True)
            print(f'Created file {html_file}.')

        else:
            print(f'No *.XLS files detected in current directory {directory}.')

    def hook(plot, element):
        '''
        Accesses Bokeh properties from within HoloViews.
        plot.state can be used to access figure properties
        plot.handles can be used to access common plot objects
        '''

        plot.state.outline_line_color = None
        # plot.state.toolbar.logo = None
        # plot.state.toolbar_location = None

    def plot_lattice_elements(data):
        '''
        Creating an additional axis to plot the lattice elements, which can be arranged on top of the other plots.
        The mapping between card types and elements is as follows:
        3  – drift
        4  – bending magnet
        5  – quadrupole
        22 – RF gap
        45 – field map (for solenoids typically)
        7  – for kickers but might also indicate diagnostics element as a marker
        '''

        # Settings used for the plots in the rings
        # BENDS: height = 2
        # QUADS: half-height = 1.2, cols = [bokeh.palettes.Reds8[2], bokeh.palettes.Blues8[1]]
        # CAVITIES: height = 1, but starting from 0.7 to represent gap, fill_color='#e7e1ef'
        # KICKERS, MONITORS: height = 2, fill_color='gray',
        # SOLENOIDS:  height = 0.5, fill_color='#a4a4a4'

        data['value'] = 0
        subdata = data.iloc[[0, -1]]
        line = subdata[['Length [m]', 'value']].hvplot(x='Length [m]', kind='line', color='#000000', line_width=1.,
                                                       height=40, responsive=True)

        # assigns an index to each type
        data['type_grp'] = (data['Card Type'].diff(1) != 0).astype('int').cumsum()

        # determines end position and length of each element
        elements = pd.DataFrame({'end_position': data.groupby('type_grp')['Length [m]'].last(),
                                 'type': data.groupby('type_grp')['Card Type'].last()}).drop(index=[1, 2]).reset_index(
            drop=True)

        elements['length'] = elements['end_position'].diff()
        elements.loc[0, 'length'] = elements['end_position'].iloc[0]

        # drops drifts (and for the moment also quadrupoles - waiting for how to identify F and D quads) from the data structure
        elements = elements.drop(elements.query('type == 3 or type == 5').index).reset_index(drop=True)

        # modify lengths in order to plot zero-length elements
        elements.loc[elements.query('length == 0.0')['length'].index, 'length'] = 0.001

        elements['start_position'] = elements['end_position'] - elements['length']
        elements['start_height'] = 0.
        elements['end_height'] = 0.
        elements['color'] = 'w'

        element_properties = {4: (2, 'green'), 5: (1.2, 'red'), 6: (2, 'gray'), 7: (2, 'gray'), 22: (2.4, 'gray'), 46: (0.5, '#a4a4a4')}

        for k in element_properties:
            elements.loc[elements['type'] == k, 'start_height'] = -element_properties[k][0] / 2
            elements.loc[elements['type'] == k, 'end_height'] = element_properties[k][0] / 2
            elements.loc[elements['type'] == k, 'color'] = element_properties[k][1]

        boxes = hv.Rectangles(elements, kdims=['start_position', 'start_height', 'end_position', 'end_height'],
                              vdims=['color']).opts(color='color')

        plot = line * boxes * line

        plot.opts(xaxis=None, yaxis=None, hooks=[webtools.hook])

        return plot

    def renderfile(dirnames, name, template, data):
        basedir = ''

        for dirname in dirnames:
            basedir = os.path.join(basedir, dirname)

        fullname = os.path.join(basedir, name)

        with open(fullname, 'w') as indexfile:
            indexfile.write(template.render(**data))

        print("Successfully created " + fullname)