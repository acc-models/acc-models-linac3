import glob
from webtools import webtools
import jinja2

# apply the custom theme to all plots
webtools.theme_it()

dirs = sorted(glob.glob('sections/*/'))
sections = []
for d in dirs:
    webtools.create_Linac3_plots(d)
    sections.append(d.rsplit('/', 2)[-2])

# generate the navigation structure, which defines the layout of the website (menu bar on the left)
# jinja is used to fill the nav.yml.template with the information from the imported directory structure
rdata = {'sections': sections}
templateLoader = jinja2.FileSystemLoader( searchpath="_scripts/web/templates/" )
templateEnv = jinja2.Environment(loader=templateLoader )
tyml = templateEnv.get_template('nav.yml.template')

webtools.renderfile(['./'], 'nav.yml', tyml, rdata)
