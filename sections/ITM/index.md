---
template: overrides/main.html
---

<h1> ITM </h1>

The ITM line is between the RFQ and the IH structure.

It provides the matching betwen the two accelerating systems in the 3 planes.

Four quadrupoles provide the transverse matching, while the buncher is used to
make a phase rotation in the longitudinal plane and match to the following IH structure.

## Transmission and energy

..and here...

<object width="100%" height="670" data="transmission_energy.html"></object> 

## RMS beam size


<object width="100%" height="320" data="RMS_beam_size.html"></object> 

## RMS beam emittance


<object width="100%" height="620" data="RMS_emittance.html"></object> 

## Phase space and profiles at the SEM grids

<<<<<<< HEAD
![image](ITM.MSG04.png)

## Layout

The layout of the ITM is from the contruction and was last updated in 1994 - from [EDMS](https://edms.cern.ch/document/463505/).
However the line has not been modified since this time.

![ITM Layout](Linac3_ITM_Layout.png){: style="width:35%"}



=======
![image](ITM.MSG04.png)
>>>>>>> e196a9d9eaf6e813969fa46301e77ae97d7c5dc3
