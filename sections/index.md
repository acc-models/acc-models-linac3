---
template: overrides/main.html
---

# Sections of Linac3

Linear accelerator 3 (Linac 3) is the starting point for the ions used in experiments at CERN. It provides lead ions for the Large Hadron Collider (LHC) and for fixed-target experiments. In the past it has also produced other ion types, including argon and xenon.

The linac comprises the following items.

- A 14.5GHz ECR ion source. 
- A low energy filter section to provide ion ion type for acceleration.
- A 101MHz RFQ accelerating to 250keV/u.
- Three IH structre cavities, accelerating up to 4.2MeV/u.
- A foil stripper to increase the charge state.
- A selection chicane to provide ion ion type after stripping.
- Three RF cavities that provide energy ramping.

The linac was commissioned in 1994 for use with the PSB, and from 2005 has supplied beam to LEIR.

![Linac3 Layout](Linac3_Layout.png){: style="width:90%"}

