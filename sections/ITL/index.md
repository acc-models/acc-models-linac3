---
template: overrides/main.html
---

<h1> ITL </h1>

Take the beam from the ion source and transport it to the RFQ.
In the line, multiple charge states are transported, and filtered by the spectrometer.


## Transmission and energy

The energy is calculated across all charge states, and as they are lost in the spectrometer
the energy changes.

<object width="100%" height="670" data="transmission_energy.html"></object> 

## RMS beam size

The beam size is the rms position of all charge states, so as different charge states are
lost, the beam size can make abrupt changes.

<object width="100%" height="320" data="RMS_beam_size.html"></object> 

## RMS beam emittance

..

<object width="100%" height="620" data="RMS_emittance.html"></object> 


## Layout

The layout is from the contructionm and was last updated in 1994 - from [EDMS](https://edms.cern.ch/document/463505/).

![ITL Layout](Linac3_ITL_Layout.png){: style="width:55%"}


## Apertures

The aperture diagram contains inner dimensions of the beam envelope (as a diameter), in millimeters.

It has not been updated to take into account the increased aperture in the first solenoid.

![ITL Aperture Diagram](Linac3_ITL_Apertures.png){: style="width:90%"}

