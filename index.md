---
template: overrides/main.html
---

<h1> Linac3 Optics Repository </h1>

This website contains the official optics models for the CERN Linac3. For each section of the Linac simulation input and output files 
and plots of the most important beam and machine parameters are available. Furthermore, the repository is available on Gitlab, AFS and EOS and can be accessed in the way described below. 

!!! note "Locations of the repository on Gitlab, AFS and EOS"
		1) A local copy of the repository on your computer can be obtained by cloning the Gitlab repository using the following syntax:

			git clone https://gitlab.cern.ch/acc-models/acc-models-linac3.git

		2) The repository is also accessible on AFS:

			/afs/cern.ch/eng/acc-models/linac3/

		3) The repository is also accessible on EOS, where all interactive plots can be found in addition to the data available on AFS:

			/eos/project/a/acc-models/public/linac3/
